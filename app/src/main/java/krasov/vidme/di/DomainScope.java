package krasov.vidme.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by user on 19.08.2017.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DomainScope {
}
