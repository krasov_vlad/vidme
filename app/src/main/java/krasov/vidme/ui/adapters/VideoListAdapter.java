package krasov.vidme.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.vidme.R;
import krasov.vidme.data.network.model.ApiResponse;
import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoHolder> {
    private static final String TAG = Constants.TAG_PREFIX + "VideoListAdapter";
    private List<ApiResponse.Video> videoList = new ArrayList<>();
    private Context context;

    public VideoListAdapter(List<ApiResponse.Video> videoList) {
        this.videoList = videoList;
    }

    public VideoListAdapter() {
    }

    public void addVideos(List<ApiResponse.Video> videos) {
        videoList.addAll(videos);
        notifyDataSetChanged();
    }

    public void clearVideos() {
        videoList.clear();
        notifyDataSetChanged();
    }

    @Override
    public VideoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_list_item, parent, false);
        context = parent.getContext();
        return new VideoHolder(v);
    }

    @Override
    public void onBindViewHolder(VideoHolder holder, int position) {
        Picasso.with(context)
                .load(videoList.get(position).thumbnailUrl)
                .into(holder.thumbnail);
        holder.videoTitle.setText(videoList.get(position).title);
        holder.videoLikesCount.setText(String.valueOf(videoList.get(position).likesCount));
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    class VideoHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.video_title)
        TextView videoTitle;
        @BindView(R.id.video_likes_count)
        TextView videoLikesCount;

        VideoHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
