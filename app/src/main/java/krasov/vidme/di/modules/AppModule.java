package krasov.vidme.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import krasov.vidme.data.SessionManager;
import krasov.vidme.domain.FeaturedNewInteractor;
import krasov.vidme.domain.FeedInteractor;
import krasov.vidme.ui.adapters.CustomPagerAdapter;
import krasov.vidme.utils.CommonUtils;

/**
 * Created by user on 15.08.2017.
 */

@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    @NonNull
    @Singleton
    Context prodidesContext() {
        return mContext;
    }

    @Provides
    @Singleton
    CommonUtils providesCommonUtils(Context context) {
        return new CommonUtils(context);
    }

    @Provides
    @Singleton
    FeaturedNewInteractor provideFeaturedInteractor() {
        return new FeaturedNewInteractor();
    }

    @Provides
    @Singleton
    FeedInteractor provideFeedInteractor() {
        return new FeedInteractor();
    }

    @Provides
    @Singleton
    SessionManager sessionManager(Context context) {
        return new SessionManager(context);
    }


}
