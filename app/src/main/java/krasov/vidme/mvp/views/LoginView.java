package krasov.vidme.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * Created by user on 06.10.2017.
 */

public interface LoginView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void showToast(int stringId);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void authSuccess();


}
