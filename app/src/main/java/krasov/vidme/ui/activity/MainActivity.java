package krasov.vidme.ui.activity;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.vidme.R;
import krasov.vidme.mvp.presenters.MainPresenter;
import krasov.vidme.mvp.views.MainView;
import krasov.vidme.ui.adapters.CustomPagerAdapter;
import krasov.vidme.utils.Constants;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    private static final String TAG = Constants.TAG_PREFIX + "MainActivity";

    @InjectPresenter
    MainPresenter mainPresenter;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    private CustomPagerAdapter customPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        customPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        initViewPager();
        initTabLayout();
    }


    private void initTabLayout() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.menu_feature));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.menu_new));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.menu_feed));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void initViewPager() {
        pager.setAdapter(customPagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }
}
