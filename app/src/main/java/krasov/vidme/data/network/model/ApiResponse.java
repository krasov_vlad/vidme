package krasov.vidme.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 06.10.2017.
 */

public class ApiResponse {
    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("page")
    @Expose
    public Page page;
    @SerializedName("videos")
    @Expose
    public List<Video> videos = null;
    @SerializedName("watching")
    @Expose
    public Watching watching;
    @SerializedName("viewerVotes")
    @Expose
    public List<Object> viewerVotes = null;

    public class Channel {

        @SerializedName("channel_id")
        @Expose
        public String channelId;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("date_created")
        @Expose
        public String dateCreated;
        @SerializedName("is_default")
        @Expose
        public boolean isDefault;
        @SerializedName("hide_suggest")
        @Expose
        public boolean hideSuggest;
        @SerializedName("show_unmoderated")
        @Expose
        public boolean showUnmoderated;
        @SerializedName("nsfw")
        @Expose
        public boolean nsfw;
        @SerializedName("follower_count")
        @Expose
        public int followerCount;
        @SerializedName("video_count")
        @Expose
        public int videoCount;
        @SerializedName("full_url")
        @Expose
        public String fullUrl;
        @SerializedName("avatar_url")
        @Expose
        public Object avatarUrl;
        @SerializedName("avatar_ai")
        @Expose
        public Object avatarAi;
        @SerializedName("cover_url")
        @Expose
        public String coverUrl;
        @SerializedName("cover_ai")
        @Expose
        public String coverAi;

    }

    public class Format {

        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("uri")
        @Expose
        public String uri;
        @SerializedName("width")
        @Expose
        public Object width;
        @SerializedName("height")
        @Expose
        public Object height;
        @SerializedName("version")
        @Expose
        public int version;

    }

    public class Page {

        @SerializedName("offset")
        @Expose
        public int offset;
        @SerializedName("limit")
        @Expose
        public int limit;
        @SerializedName("total")
        @Expose
        public int total;
        @SerializedName("currentMarker")
        @Expose
        public Object currentMarker;
        @SerializedName("currentMarkerDay")
        @Expose
        public Object currentMarkerDay;
        @SerializedName("currentMarkerDate")
        @Expose
        public Object currentMarkerDate;
        @SerializedName("nextMarker")
        @Expose
        public String nextMarker;

    }

    public class User {

        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("username")
        @Expose
        public String username;
        @SerializedName("full_url")
        @Expose
        public String fullUrl;
        @SerializedName("avatar")
        @Expose
        public String avatar;
        @SerializedName("avatar_url")
        @Expose
        public String avatarUrl;
        @SerializedName("avatar_ai")
        @Expose
        public String avatarAi;
        @SerializedName("cover")
        @Expose
        public String cover;
        @SerializedName("cover_url")
        @Expose
        public String coverUrl;
        @SerializedName("cover_ai")
        @Expose
        public String coverAi;
        @SerializedName("displayname")
        @Expose
        public Object displayname;
        @SerializedName("follower_count")
        @Expose
        public int followerCount;
        @SerializedName("likes_count")
        @Expose
        public String likesCount;
        @SerializedName("video_count")
        @Expose
        public int videoCount;
        @SerializedName("video_views")
        @Expose
        public String videoViews;
        @SerializedName("videos_scores")
        @Expose
        public int videosScores;
        @SerializedName("comments_scores")
        @Expose
        public int commentsScores;
        @SerializedName("bio")
        @Expose
        public String bio;
        @SerializedName("enabled")
        @Expose
        public String enabled;
        @SerializedName("ga_id")
        @Expose
        public Object gaId;
        @SerializedName("sub_enabled")
        @Expose
        public boolean subEnabled;
        @SerializedName("disallow_downloads")
        @Expose
        public boolean disallowDownloads;
        @SerializedName("vip")
        @Expose
        public boolean vip;

    }

    public class Video {

        @SerializedName("video_id")
        @Expose
        public String videoId;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("original_size")
        @Expose
        public String originalSize;
        @SerializedName("full_url")
        @Expose
        public String fullUrl;
        @SerializedName("embed_url")
        @Expose
        public String embedUrl;
        @SerializedName("user_id")
        @Expose
        public String userId;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("duration")
        @Expose
        public float duration;
        @SerializedName("height")
        @Expose
        public int height;
        @SerializedName("width")
        @Expose
        public int width;
        @SerializedName("date_created")
        @Expose
        public String dateCreated;
        @SerializedName("date_stored")
        @Expose
        public String dateStored;
        @SerializedName("date_completed")
        @Expose
        public String dateCompleted;
        @SerializedName("comment_count")
        @Expose
        public int commentCount;
        @SerializedName("view_count")
        @Expose
        public int viewCount;
        @SerializedName("share_count")
        @Expose
        public int shareCount;
        @SerializedName("version")
        @Expose
        public int version;
        @SerializedName("nsfw")
        @Expose
        public boolean nsfw;
        @SerializedName("moderated")
        @Expose
        public boolean moderated;
        @SerializedName("thumbnail")
        @Expose
        public String thumbnail;
        @SerializedName("thumbnail_url")
        @Expose
        public String thumbnailUrl;
        @SerializedName("thumbnail_gif")
        @Expose
        public Object thumbnailGif;
        @SerializedName("thumbnail_gif_url")
        @Expose
        public Object thumbnailGifUrl;
        @SerializedName("thumbnail_ai")
        @Expose
        public String thumbnailAi;
        @SerializedName("storyboard")
        @Expose
        public String storyboard;
        @SerializedName("score")
        @Expose
        public int score;
        @SerializedName("likes_count")
        @Expose
        public int likesCount;
        @SerializedName("channel_id")
        @Expose
        public String channelId;
        @SerializedName("source")
        @Expose
        public String source;
        @SerializedName("private")
        @Expose
        public boolean _private;
        @SerializedName("scheduled")
        @Expose
        public boolean scheduled;
        @SerializedName("subscribed_only")
        @Expose
        public boolean subscribedOnly;
        @SerializedName("date_published")
        @Expose
        public String datePublished;
        @SerializedName("latitude")
        @Expose
        public int latitude;
        @SerializedName("longitude")
        @Expose
        public int longitude;
        @SerializedName("place_id")
        @Expose
        public Object placeId;
        @SerializedName("place_name")
        @Expose
        public Object placeName;
        @SerializedName("colors")
        @Expose
        public String colors;
        @SerializedName("reddit_link")
        @Expose
        public Object redditLink;
        @SerializedName("youtube_override_source")
        @Expose
        public Object youtubeOverrideSource;
        @SerializedName("complete")
        @Expose
        public Object complete;
        @SerializedName("complete_url")
        @Expose
        public String completeUrl;
        @SerializedName("watching_count")
        @Expose
        public int watchingCount;
        @SerializedName("hot_score")
        @Expose
        public float hotScore;
        @SerializedName("is_featured")
        @Expose
        public boolean isFeatured;
        @SerializedName("date_featured")
        @Expose
        public String dateFeatured;
        @SerializedName("user")
        @Expose
        public User user;
        @SerializedName("channel")
        @Expose
        public Channel channel;
        @SerializedName("formats")
        @Expose
        public List<Format> formats = null;
        @SerializedName("total_tipped")
        @Expose
        public int totalTipped;

    }

    public class Watching {

        @SerializedName("18101505")
        @Expose
        public int _18101505;
        @SerializedName("18109374")
        @Expose
        public int _18109374;
        @SerializedName("18112308")
        @Expose
        public int _18112308;
        @SerializedName("18120717")
        @Expose
        public int _18120717;
        @SerializedName("18110649")
        @Expose
        public int _18110649;
        @SerializedName("18105624")
        @Expose
        public int _18105624;
        @SerializedName("18108012")
        @Expose
        public int _18108012;
        @SerializedName("18117318")
        @Expose
        public int _18117318;
        @SerializedName("18102399")
        @Expose
        public int _18102399;

    }
}
