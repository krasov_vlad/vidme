package krasov.vidme.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import java.net.UnknownHostException;

import javax.inject.Inject;

import krasov.vidme.App;
import krasov.vidme.R;
import krasov.vidme.domain.FeaturedNewInteractor;
import krasov.vidme.mvp.views.VideoListView;
import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */
@InjectViewState
public class VideoListPresenter extends BasePresenter<VideoListView> {
    private static final String TAG = Constants.TAG_PREFIX + "FPresenter";
    @Inject
    FeaturedNewInteractor featuredNewInteractor;
    private int type;

    public VideoListPresenter(int type) {
        App.getAppComponent().inject(this);
        this.type = type;
    }

    @Override
    public void attachView(VideoListView view) {
        super.attachView(view);
        Log.d(TAG, "attachFeatured");

    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void getVideos(int limit, int offset) {
        addSubscription(featuredNewInteractor.getVideos(type, limit, offset)
                .subscribe(
                        videos -> getViewState().addVideoToList(videos),
                        throwable -> {
                            Log.d(TAG, String.valueOf(throwable.getClass()));
                            getViewState().errorLoad();
                            if (throwable.getClass() == UnknownHostException.class) {
                                getViewState().showToast(R.string.networkerror);
                            }
                        }));
    }

}
