package krasov.vidme.domain;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import krasov.vidme.App;
import krasov.vidme.data.network.VidMeService;
import krasov.vidme.data.network.model.AuthResponse;

/**
 * Created by user on 06.10.2017.
 */

public class FeedInteractor {
    @Inject
    VidMeService vidMeService;

    public FeedInteractor() {
        App.getDomainComponent().inject(this);
    }

    public Single<String> auth(String userName, String password) {
        return vidMeService.auth(userName, password)
                .map(authResponse -> authResponse.auth.token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
