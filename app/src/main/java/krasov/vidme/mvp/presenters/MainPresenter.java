package krasov.vidme.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import krasov.vidme.App;
import krasov.vidme.R;
import krasov.vidme.data.SessionManager;
import krasov.vidme.mvp.views.MainView;
import krasov.vidme.utils.Constants;


/**
 * Created by user on 15.08.2017.
 */

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {
    private static final String TAG = Constants.TAG_PREFIX + "MainPresenter";
    @Inject
    SessionManager sessionManager;

    public MainPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }


}
