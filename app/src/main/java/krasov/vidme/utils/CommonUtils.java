package krasov.vidme.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by user on 15.08.2017.
 */

public class CommonUtils {
    private static final String TAG = Constants.TAG_PREFIX + "CommonUtils";
    private Context context;

    public CommonUtils(Context context) {
        this.context = context;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


}
