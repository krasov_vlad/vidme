package krasov.vidme.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import javax.inject.Inject;

import krasov.vidme.App;
import krasov.vidme.data.SessionManager;
import krasov.vidme.ui.fragment.VideoListFragment;
import krasov.vidme.ui.fragment.LoginFragment;
import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */

public class CustomPagerAdapter extends FragmentPagerAdapter {
    private static final String TAG = Constants.TAG_PREFIX + "CPA";
    @Inject
    SessionManager sessionManager;

    public static final int F_COUNT = 3;

    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
        App.getAppComponent().inject(this);
    }


    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem " + position + " " + sessionManager.isLoggedIn());
        switch (position) {
            case 0:
                return VideoListFragment.getInstance(Constants.TYPE_FEATURE);
            case 1:
                return VideoListFragment.getInstance(Constants.TYPE_NEW);
            case 2:
                if (sessionManager.isLoggedIn())
                    return VideoListFragment.getInstance(Constants.TYPE_FEED);
                else
                    return LoginFragment.getInstance();
            default:
                return VideoListFragment.getInstance(Constants.TYPE_FEATURE);
        }
    }

    @Override
    public int getCount() {
        return F_COUNT;
    }
}
