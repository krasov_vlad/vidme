package krasov.vidme.di.modules;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import dagger.Module;
import dagger.Provides;
import krasov.vidme.data.network.VidMeApi;
import krasov.vidme.data.network.VidMeService;
import krasov.vidme.di.DomainScope;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 17.08.2017.
 */

@Module
public class RetrofitModule {

    @Provides
    @DomainScope
    Retrofit provideRetrofit(Retrofit.Builder builder) {
        return builder.baseUrl("https://api.vid.me").build();
    }

    @Provides
    @DomainScope
    Retrofit.Builder provideRetrofitBuilder(Converter.Factory converterFactory) {
        return new Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    }

    @Provides
    @DomainScope
    Converter.Factory provideConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @DomainScope
    VidMeApi provideVidMeApi(Retrofit retrofit) {
        return retrofit.create(VidMeApi.class);
    }

    @Provides
    @DomainScope
    VidMeService providesService(VidMeApi vidMeApi) {
        return new VidMeService(vidMeApi);
    }

}
