package krasov.vidme.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.vidme.R;
import krasov.vidme.mvp.presenters.LoginPresenter;
import krasov.vidme.mvp.views.LoginView;
import krasov.vidme.ui.activity.MainActivity;
import krasov.vidme.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends MvpAppCompatFragment implements LoginView {
    @InjectPresenter
    LoginPresenter loginPresenter;
    @BindView(R.id.passwordET)
    EditText passwordET;
    @BindView(R.id.loginET)
    EditText loginET;
    @BindView(R.id.loginButton)
    Button loginButton;

    public LoginFragment() {
        // Required empty public constructor
    }

    public static LoginFragment getInstance() {
        LoginFragment feedFragment = new LoginFragment();

        return feedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        loginButton.setOnClickListener(v -> {
            loginPresenter.auth(loginET.getText().toString(), passwordET.getText().toString());
        });
    }

    @Override
    public void showToast(int stringId) {
        Toast.makeText(getContext(), stringId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void authSuccess() {
        //todo swap fragment to featrued new
    }
}
