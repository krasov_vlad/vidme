package krasov.vidme.data.network;

import io.reactivex.Observable;
import io.reactivex.Single;
import krasov.vidme.data.network.model.ApiResponse;
import krasov.vidme.data.network.model.AuthResponse;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by user on 19.08.2017.
 */

public interface VidMeApi {
    @POST("/auth/create")
    Single<AuthResponse> createAuth(@Query("username") String username, @Query("password") String password);

//    @POST("/auth/delete")
//    Observable<Boolean> deleteAuth(@Query("AccessToken") String token);
//

    @GET("/videos/featured")
    Observable<ApiResponse> featuredVideo(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/videos/new")
    Observable<ApiResponse> newVideo(@Query("limit") int limit, @Query("offset") int offset);

    @GET("/videos/feed")
    Observable<ApiResponse> feedVideo(@Query("limit") int limit, @Query("offset") int offset, @Query("token") String token);
}
