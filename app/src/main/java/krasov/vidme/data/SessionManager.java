package krasov.vidme.data;

import android.content.Context;
import android.content.SharedPreferences;

import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */

public class SessionManager {
    private static final String PREF_NAME = "PREFS";
    private static final int PRIVATE_MODE = 0;
    private static String TAG = Constants.TAG_PREFIX + "SessionManager";
    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";
    private static final String KEY_TOKEN = "token";
    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;

    Context _context;

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void setToken(String token) {
        editor.putString(KEY_TOKEN, token);
        editor.commit();
    }

    public String getToken() {
        return pref.getString(KEY_TOKEN, "");
    }


}
