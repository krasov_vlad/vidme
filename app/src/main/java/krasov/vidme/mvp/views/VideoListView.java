package krasov.vidme.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import krasov.vidme.data.network.model.ApiResponse;

/**
 * Created by user on 06.10.2017.
 */

public interface VideoListView extends MvpView {

    @StateStrategyType(SingleStateStrategy.class)
    void addVideoToList(List<ApiResponse.Video> videos);

    @StateStrategyType(SkipStrategy.class)
    void errorLoad();

    @StateStrategyType(SkipStrategy.class)
    void showToast(int stringId);

}
