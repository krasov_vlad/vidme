package krasov.vidme.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import krasov.vidme.R;
import krasov.vidme.data.network.model.ApiResponse;
import krasov.vidme.mvp.presenters.VideoListPresenter;
import krasov.vidme.mvp.views.VideoListView;
import krasov.vidme.ui.adapters.VideoListAdapter;
import krasov.vidme.utils.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoListFragment extends MvpAppCompatFragment implements VideoListView {
    private static final String TAG = Constants.TAG_PREFIX + "FNFragment";
    @InjectPresenter
    VideoListPresenter videoListPresenter;

    @ProvidePresenter
    VideoListPresenter provideFeaturedNewPresenter() {
        return new VideoListPresenter(getArguments().getInt(Constants.TYPE_KEY));
    }

    @BindView(R.id.featuredRV)
    RecyclerView featuredRV;
    @BindView(R.id.featuredSwipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.loadMoreProgress)
    ProgressBar progressBar;

    private VideoListAdapter videoListAdapter;
    private int offset = 0;

    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    public VideoListFragment() {

    }

    public static VideoListFragment getInstance(int typeFN) {
        VideoListFragment featuredFragment = new VideoListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TYPE_KEY, typeFN);
        featuredFragment.setArguments(bundle);
        return featuredFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        configRecyclerView();
        configSwipeRefreshLayout();
        loadMore(0);


    }

    private void configSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(() -> {
            videoListAdapter.clearVideos();
            offset = 0;
            loadMore(offset);
        });

    }

    private void configRecyclerView() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        featuredRV.setLayoutManager(linearLayoutManager);
        videoListAdapter = new VideoListAdapter();
        featuredRV.setAdapter(videoListAdapter);
        featuredRV.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            progressBar.setVisibility(View.VISIBLE);
                            loadMore(offset += Constants.LIMIT);
                        }
                    }
                }
            }
        });
    }

    private void loadMore(int offset) {
        Log.d(TAG, "loadMore");
        videoListPresenter.getVideos(Constants.LIMIT, offset);
    }

    @Override
    public void addVideoToList(List<ApiResponse.Video> videos) {
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        videoListAdapter.addVideos(videos);
        loading = true;
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void errorLoad() {
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
        loading = true;
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToast(int stringId) {
        Toast.makeText(getContext(), stringId, Toast.LENGTH_SHORT).show();
    }
}
