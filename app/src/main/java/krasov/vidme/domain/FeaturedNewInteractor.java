package krasov.vidme.domain;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import krasov.vidme.App;
import krasov.vidme.data.SessionManager;
import krasov.vidme.data.network.VidMeService;
import krasov.vidme.data.network.model.ApiResponse;
import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */

public class FeaturedNewInteractor {
    @Inject
    VidMeService vidMeService;
    @Inject
    SessionManager sessionManager;

    public FeaturedNewInteractor() {
        App.getDomainComponent().inject(this);
    }

    public Observable<List<ApiResponse.Video>> getVideos(int type, int limit, int offset) {
        if (type == Constants.TYPE_NEW)
            return vidMeService.getNewVideos(limit, offset)
                    .map(apiResponse -> apiResponse.videos)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        else if (type == Constants.TYPE_FEED) {
            return vidMeService.getFeedVideos(limit, offset, sessionManager.getToken())
                    .map(apiResponse -> apiResponse.videos)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        } else {
            return vidMeService.getFeaturedVideos(limit, offset)
                    .map(apiResponse -> apiResponse.videos)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    }

}
