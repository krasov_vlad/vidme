package krasov.vidme.di;

import javax.inject.Singleton;

import dagger.Component;
import krasov.vidme.di.modules.AppModule;
import krasov.vidme.di.modules.RetrofitModule;
import krasov.vidme.mvp.presenters.VideoListPresenter;
import krasov.vidme.mvp.presenters.LoginPresenter;
import krasov.vidme.mvp.presenters.MainPresenter;
import krasov.vidme.ui.adapters.CustomPagerAdapter;


/**
 * Created by user on 15.08.2017.
 */

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {
    DomainComponent plusDomainComponent(RetrofitModule retrofitModule);

    void inject(MainPresenter mainPresenter);

    void inject(VideoListPresenter presenter);

    void inject(LoginPresenter presenter);

    void inject(CustomPagerAdapter customPagerAdapter);

}
