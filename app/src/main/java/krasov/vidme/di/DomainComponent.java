package krasov.vidme.di;

import dagger.Subcomponent;
import krasov.vidme.di.modules.RetrofitModule;
import krasov.vidme.domain.FeaturedNewInteractor;
import krasov.vidme.domain.FeedInteractor;

/**
 * Created by user on 19.08.2017.
 */

@Subcomponent(modules = {RetrofitModule.class})
@DomainScope
public interface DomainComponent {
    void inject(FeaturedNewInteractor featuredNewInteractor);
    void inject(FeedInteractor feedInteractor);
}
