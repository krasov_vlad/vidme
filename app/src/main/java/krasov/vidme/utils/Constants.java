package krasov.vidme.utils;

/**
 * Created by user on 15.08.2017.
 */

public interface Constants {
    String TAG_PREFIX = "MYTAG_";
    int LIMIT = 10;
    int TYPE_FEATURE = 0;
    int TYPE_NEW = 1;
    int TYPE_FEED = 3;

    String TYPE_KEY = "type";
}
