package krasov.vidme.data.network;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import krasov.vidme.data.network.VidMeApi;
import krasov.vidme.data.network.model.ApiResponse;
import krasov.vidme.data.network.model.AuthResponse;

/**
 * Created by user on 19.08.2017.
 */

public class VidMeService {
    private VidMeApi vidMeApi;

    public VidMeService(VidMeApi vidMeApi) {
        this.vidMeApi = vidMeApi;
    }

    public Observable<ApiResponse> getFeaturedVideos(int limit, int offset) {
        return vidMeApi.featuredVideo(limit, offset);
    }

    public Observable<ApiResponse> getNewVideos(int limit, int offset) {
        return vidMeApi.newVideo(limit, offset);
    }

    public Observable<ApiResponse> getFeedVideos(int limit, int offset, String token) {
        return vidMeApi.feedVideo(limit, offset, token);
    }

    public Single<AuthResponse> auth(String userName, String password) {
        return vidMeApi.createAuth(userName, password);
    }
}
