package krasov.vidme.mvp.presenters;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import krasov.vidme.App;
import krasov.vidme.R;
import krasov.vidme.data.SessionManager;
import krasov.vidme.domain.FeedInteractor;
import krasov.vidme.mvp.views.LoginView;
import krasov.vidme.utils.Constants;

/**
 * Created by user on 06.10.2017.
 */
@InjectViewState
public class LoginPresenter extends BasePresenter<LoginView> {
    private static final String TAG = Constants.TAG_PREFIX + "LoginPresenter";
    @Inject
    FeedInteractor feedInteractor;
    @Inject
    SessionManager sessionManager;

    public LoginPresenter() {
        App.getAppComponent().inject(this);
    }

    @Override
    public void attachView(LoginView view) {
        super.attachView(view);
        Log.d(TAG, "attachFeed");
    }


    public void auth(String username, String password) {
        if (username.length() < 4 || password.length() < 4) {
            getViewState().showToast(R.string.logingpassoword_small);
        } else {
            feedInteractor.auth(username, password)
                    .subscribe(new SingleObserver<String>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(String s) {
                            Log.d(TAG, s);
                            getViewState().authSuccess();
                            sessionManager.setLogin(true);
                            sessionManager.setToken(s);
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, e.toString());
                        }
                    });
        }
    }

}
