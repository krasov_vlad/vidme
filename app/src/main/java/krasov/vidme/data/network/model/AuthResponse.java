package krasov.vidme.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 06.10.2017.
 */

public class AuthResponse {

    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("auth")
    @Expose
    public Auth auth;
    @SerializedName("user")
    @Expose
    public ApiResponse.User user;

    public class Auth {

        @SerializedName("token")
        @Expose
        public String token;
        @SerializedName("expires")
        @Expose
        public String expires;
        @SerializedName("user_id")
        @Expose
        public String userId;

    }

}