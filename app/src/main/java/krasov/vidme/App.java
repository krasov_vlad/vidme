package krasov.vidme;

import android.app.Application;

import krasov.vidme.di.AppComponent;
import krasov.vidme.di.DaggerAppComponent;
import krasov.vidme.di.DomainComponent;
import krasov.vidme.di.modules.AppModule;
import krasov.vidme.di.modules.RetrofitModule;


/**
 * Created by user on 15.08.2017.
 */

public class App extends Application {
    private static AppComponent appComponent;
    private static DomainComponent domainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = buildAppComponent();
        domainComponent = buildDomainComponent();
    }

    public static DomainComponent buildDomainComponent() {
        domainComponent = appComponent.plusDomainComponent(new RetrofitModule());
        return domainComponent;
    }

    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }


    public static AppComponent getAppComponent() {
        return appComponent;
    }

    public static DomainComponent getDomainComponent() {
        return domainComponent;
    }

}
